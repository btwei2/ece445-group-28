//Created by Ben Wei on 4/5/23 for ECE445 Group Project
//Single Servo Test

#include <Servo.h>

int speed = 60; //ranges from 0(min) to 90(max)
int interval = 5000; //delay in ms

Servo PWM1;

void setup() {
  PWM1.attach(9,1300,1700); //9 is marked PB1 on the ATmega328-P
}

void loop() {
  delay(interval);
  PWM1.write(90); //Still
  delay(interval);
  PWM1.write(90+speed); //Forward
  delay(interval);
  PWM1.write(90); //Still
  delay(interval);
  PWM1.write(90-speed); //Backwards
}

//Notes:
//Servo.write(90) = Servo.writeMicroseconds(1500);
//This works because we want 1.5 ms width for the 900-00008

//Verifications:
//Run Program as is
//Configure Speed variable to reasonable (not too fast)
//Configure Delay variable to allow enough time for a full roll/unroll

//Other ideas:
//Calibrate the servo if it is drifting (not still during Still), see 900-00008 datasheet