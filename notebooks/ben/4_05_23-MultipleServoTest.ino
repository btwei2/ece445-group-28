//MultipleServo Test

#include <Servo.h>

int spd = 60;
int duration = 1000;

Servo PWM1;
Servo PWM2;
Servo PWM3;
Servo PWM4;

void setup() {
  PWM1.attach(9,1300,1700);
  PWM2.attach(10,1300,1700);
  PWM3.attach(3,1300,1700);
  PWM4.attach(5,1300,1700);
}

void loop() {
  delay(1000);
  DeployAll();
  delay(1000);
  RecallAll();
}

void DeployAll(){
  PWM1.write(90+speed);
  PWM2.write(90+speed);
  PWM3.write(90+speed);
  PWM4.write(90+speed);
  delay(duration);
  PWM1.write(90);
  PWM2.write(90);
  PWM3.write(90);
  PWM4.write(90);
}

void RecallAll(){
  PWM1.write(90-speed);
  PWM2.write(90-speed);
  PWM3.write(90-speed);
  PWM4.write(90-speed);
  delay(duration);
  PWM1.write(90);
  PWM2.write(90);
  PWM3.write(90);
  PWM4.write(90);
}